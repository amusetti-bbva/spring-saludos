package mis.pruebas.borramespring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BorrameSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(BorrameSpringApplication.class, args);
	}

}
