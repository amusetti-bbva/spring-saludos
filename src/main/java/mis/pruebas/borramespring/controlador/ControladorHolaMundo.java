package mis.pruebas.borramespring.controlador;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${api.saludos.v1}")
public class ControladorHolaMundo {

    private String saludo = "Hola";

    //@RequestMapping(method = RequestMethod.GET, path =  "/api-hola/v1/saludos")
    @GetMapping
     public String saludar(
            @RequestParam(name = "saludar", required = false) String nombre
    ) {
        if(nombre == null || nombre.isEmpty())
            nombre = "Mundo";
        if(saludo == null || saludo.isEmpty())
            saludo = "Hola";

        if(nombre.equalsIgnoreCase("Voldemort")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return String.format("¡%s, %s!", saludo, nombre);
    }

    @PostMapping
    public void agregarSaludo(@RequestBody String saludo) {
        this.saludo = saludo;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSaludo() {
        this.saludo = "Hola";
    }
}
